# DAZN Challenge

[![CircleCI](https://circleci.com/bb/simontabor/dazn-challenge.svg?style=svg&circle-token=feee9ae25ebd3c7ccf7eedb2ae91fa2b64c244a4)](https://circleci.com/bb/simontabor/dazn-challenge)

## Requirements

> Build a service in Node.js that exposes an API which can be consumed from any client. This service must check how many video streams a given user is watching and prevent a user watching more than 3 video streams concurrently.

## Documentation

This API is self-documented using [Swagger v2.0](https://swagger.io/docs/specification/2-0/basic-structure/). The docs are available in JSON format at [/swagger.json](https://dazn-challenge.herokuapp.com/swagger.json), or in HTML format at [/docs](https://dazn-challenge.herokuapp.com/docs).

## Usage

### Live

The API is running live on Heroku. It's available at https://dazn-challenge.herokuapp.com/.

### Setup

#### Install Redis

For local development, ensure Redis is installed and running locally.

On MacOS:
```sh
brew install redis
```

#### Install Dependencies

```sh
npm install
```

### Tests
```sh
npm test
```

### Running

```sh
npm start
```

### Deployment

Heroku deployment is managed automatically via CircleCI when code is pushed to the `master` branch.

## Assumptions
- No authentication is required as this is an internal API (and can therefore be run behind an internal ELB)
- Data loss is not business critical - if all data was lost then the streams will be re-added in time. This should mean the end users (streamers) won't experience a degraded exeperience. The worst that can happen is a user being allowed to watch too many streams for a short amount of time.


## Scaling
- The Node.js application can be scaled infinitely horizontally. For multi-core instances, [cluster](https://nodejs.org/api/cluster.html) can be added easily
- Redis is very efficient, but if it needs to be scaled we can use Redis Cluster, which [ioredis](https://github.com/luin/ioredis) supports

## Future Improvements
- The Swagger documentation (parameters etc) should be used to validate requests. This would then make the API fully self-documenting
- Test more edge-cases. At the moment there is 100% code coverage but that can often give a false sense of security.
- Update my ultra-lightweight server module ([slight](https://github.com/simontabor/slight)) to support Promises for code consistency
- Close the Redis connection gracefully when the tests finish, so Jest `forceExit` isn't required
- Upgrade to OpenAPI 3.0 (instead of Swagger/OpenAPI 2.0)
