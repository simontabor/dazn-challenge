'use strict'
const Slight = require('slight')
const bodyParser = require('body-parser')
const DaznError = require('./DaznError')
const { setupRoutes } = require('./router')

const server = new Slight({
  port: process.env.PORT || 3000,
  optional_trailing_slash: true
})

server.use(bodyParser.json())

setupRoutes(server)

server.before((req, res, next) => {
  res.setHeader('Content-Type', 'application/json; charset=utf-8')
  next()
})

server.finish((err, body, req, res) => {
  if (err) {
    // set the response body to a DaznError
    body = DaznError.handleAPIError(err, req, res)
  }

  if (res.getHeader('Content-Type') === 'text/html') {
    res.send(res.status || 200, body)
    return
  }

  const stringBody = JSON.stringify(body)
  res.send(res.status || 200, stringBody)
})

module.exports = server
