'use strict'

// require all routes
const routes = [
  ...require('./routes/streams'),
  ...require('./routes/docs')
]

const setupRoutes = server => {
  // set up each route
  routes.forEach(route => {
    server.route(route.method, route.url, [ route.handler ])
  })
}

module.exports = {
  routes,
  setupRoutes
}
