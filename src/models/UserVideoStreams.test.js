'use strict'
const UserVideoStreams = require('./UserVideoStreams')
const redis = require('../clients/redis')
const DaznError = require('../DaznError')

const NOW = 1532375448782
// set date to 2018-07-23T19:50:48.782Z for consistent testing
jest.spyOn(Date, 'now').mockImplementation(() => NOW)

beforeAll(async () => {
  // not connected to default redis, we shouldn't continue in case it's production
  if (process.env.REDIS_URL) {
    console.error(new Error(`Redis URL (${process.env.REDIS_URL}) configured during tests, not continuing just in case`))
    process.exit(1)
  }

  await redis.flushdb()
})

describe('UserVideoStreams', () => {
  const userVideoStreams = new UserVideoStreams(1)

  describe('#add', () => {
    it('adds a new stream to the list', async () => {
      const wasAdded = await userVideoStreams.add(500)
      expect(wasAdded).toBe(true)

      const score = await redis.zscore(userVideoStreams.key, 500)
      expect(Number(score)).toBe(NOW)
    })

    it('stops users from watching more than 3 streams', async () => {
      await Promise.all([
        userVideoStreams.add(501),
        userVideoStreams.add(502)
      ])

      await expect(userVideoStreams.add(503)).rejects.toThrow(new DaznError('stream_limit_reached'))
    })

    it('can "ping" a stream to keep it active', async () => {
      const wasAdded = await userVideoStreams.add(500)
      expect(wasAdded).toBe(false)
    })
  })

  describe('#list', () => {
    const emptyUserVideoStreams = new UserVideoStreams(2)
    it('returns an empty array when a user has no active streams', async () => {
      const list = await emptyUserVideoStreams.list()
      expect(list).toEqual([])
    })
  })

  describe('#expiryTime', () => {
    it('returns a JS timestamp (now - 1 hour)', () => {
      const expiry = userVideoStreams.expiryTime
      expect(expiry).toBe(NOW - (1000 * 60 * 60))
      expect(new Date(expiry)).toEqual(new Date('2018-07-23T18:50:48.782Z'))
    })
  })
})

afterAll(async () => {
  await redis.disconnect()
})
