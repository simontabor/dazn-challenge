'use strict'
const redis = require('../clients/redis')
const DaznError = require('../DaznError')
const STREAM_TIMEOUT = process.env.STREAM_TIMEOUT || 1000 * 60 * 60
const MAX_STREAMS = process.env.MAX_STREAMS || 3

class UserVideoStreams {
  constructor(userID) {
    this.userID = userID
    this.key = 'userStreams:' + userID
  }

  get expiryTime() {
    return Date.now() - STREAM_TIMEOUT
  }

  async list() {
    const activeStreams = await redis.zrangebyscore(this.key, this.expiryTime, '+inf')
    // tidy up the list, just to prevent too much redis memory usage if many streams expire
    this.removeExpired()
    return activeStreams
  }

  count() {
    return redis.zcount(this.key, this.expiryTime, '+inf')
  }

  async add(id) {
    const zaddResponse = await redis.zadd(this.key, Date.now(), id)
    const wasAdded = !!zaddResponse

    if (wasAdded) {
      const count = await this.count()
      if (count > MAX_STREAMS) {
        // immediately remove this stream, as we're not going to let the user watch it
        await this.delete(id)
        throw new DaznError('stream_limit_reached')
      }
    }

    return !!wasAdded
  }

  async delete(id) {
    const zremResponse = await redis.zrem(this.key, id)
    const wasDeleted = !!zremResponse

    if (!wasDeleted) {
      throw new DaznError('stream_not_found', { id })
    }

    return wasDeleted
  }

  removeExpired() {
    return redis.zremrangebyscore(this.key, 0, this.expiryTime)
  }
}

module.exports = UserVideoStreams
