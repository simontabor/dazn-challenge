'use strict'
const supertest = require('supertest')
const { server } = require('../server')
const request = supertest(server)

describe('/swagger.json', () => {
  describe('GET', () => {
    it('gets JSON swagger documentation', () => {
      return request.get('/swagger.json')
        .expect(200)
        .expect(res => {
          expect(res.body.swagger).toBe('2.0')
          expect(res.body).toMatchSnapshot()
        })
    })
  })
})

describe('/docs', () => {
  describe('GETS', () => {
    it('gets a HTML page to render API documentation', () => {
      return request.get('/docs')
        .expect(200)
        .expect('Content-Type', 'text/html')
    })
  })
})
