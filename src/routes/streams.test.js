'use strict'
const supertest = require('supertest')
const { server } = require('../server')
const request = supertest(server)

describe('/users/:user_id/streams', () => {
  describe('POST', () => {
    it('can add a new stream', () => {
      return request.post('/users/1000/streams')
        .send({ stream_id: 100 })
        .expect(201, { added: true })
    })

    it('errors when a user watches too many streams', async () => {
      await Promise.all([
        request.post('/users/1000/streams')
          .send({ stream_id: 101 })
          .expect(201, { added: true }),
        request.post('/users/1000/streams')
          .send({ stream_id: 102 })
          .expect(201, { added: true })
      ])

      return request.post('/users/1000/streams')
        .send({ stream_id: 103 })
        .expect(403, {
          code: 'stream_limit_reached',
          status: 403,
          message: 'Stream limit reached',
          description: 'You cannot watch more than 3 streams at a time',
          extra: {}
        })
    })

    it('can update (ping) an existing stream', () => {
      return request.post('/users/1000/streams')
        .send({ stream_id: 100 })
        .expect(200, { added: false })
    })
  })

  describe('GET', () => {
    it('responds with a list of active streams', () => {
      return request.get('/users/1000/streams')
        .expect(200)
        .expect((res) => {
          expect(res.body).toHaveLength(3)
        })
    })

    it('responds with an empty list of streams for new users', () => {
      return request.get('/users/1001/streams')
        .expect(200, [])
    })
  })
})

describe('/users/:user_id/streams/:stream_id', () => {
  describe('DELETE', () => {
    it('allows streams to be deleted', () => {
      return request.del('/users/1000/streams/100')
        .expect(200, { deleted: true })
    })

    it('responds with 404 for inactive streams', () => {
      return request.del('/users/1000/streams/999')
        .expect(404, {
          code: 'stream_not_found',
          status: 404,
          message: 'Stream not found',
          description: 'No stream was found with this ID',
          extra: { id: '999' }
        })
    })
  })
})
