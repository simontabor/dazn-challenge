'use strict'

// this is in a function do avoid cyclic dependencies
const generateSwagger = () => {
  const { routes } = require('../router')

  const swagger = {
    swagger: '2.0',
    info: {
      title: 'DAZN Stream Limiting API',
      description: 'API to monitor and limit the number of video streams a user can watch',
      contact: {
        name: 'Simon Tabor',
        email: 'me@simontabor.com'
      },
      version: '1.0.0'
    },
    basePath: '/',
    schemes: [ 'https' ],
    consumes: [ 'application/json' ],
    produces: [ 'application/json' ],
    paths: {},
    definitions: {
      Error: {
        type: 'object',
        required: [
          'code',
          'message',
          'status'
        ],
        properties: {
          code: {
            type: 'string'
          },
          status: {
            type: 'integer',
            format: 'int32',
            minimum: 400,
            maximum: 600,
            exclusiveMaximum: true
          },
          message: {
            type: 'string'
          },
          description: {
            type: 'string'
          },
          extra: {
            type: 'object'
          }
        }
      }
    }
  }

  routes.forEach(route => {
    const path = {
      summary: route.summary,
      description: route.description,
      parameters: route.parameters,
      responses: route.responses
    }

    // from :user_id to {user_id}
    const url = route.url.replace(/\/:([A-Za-z0-9_]+)(\([^\\]+?\))?/g, function(s, n) {
      return '/{' + n + '}'
    })

    if (!swagger.paths[url]) {
      swagger.paths[url] = {}
    }

    swagger.paths[url][route.method.toLowerCase()] = path
  })

  return swagger
}

module.exports = [
  {
    method: 'GET',
    url: '/swagger.json',
    summary: 'Get Swagger docs',
    description: 'Get a JSON representation of the API documentation',
    handler: (req, res, next) => {
      next(null, generateSwagger())
    }
  },
  {
    method: 'GET',
    url: '/docs',
    summary: 'Get the API docs',
    description: 'Get the full API documentation in HTML',
    handler: (req, res, next) => {
      res.setHeader('Content-Type', 'text/html')

      next(null, `
        <!DOCTYPE html>
        <html>
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta http-equiv="X-UA-Compatible" content="ie=edge">
          <title>DAZN Stream Limiting API Docs</title>
        </head>
        <body>
          <redoc spec-url='/swagger.json' lazy-rendering></redoc>
          <script src="https://rebilly.github.io/ReDoc/releases/latest/redoc.min.js"></script>
        </body>
        </html>
      `)
    }
  }
]
