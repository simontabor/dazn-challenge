'use strict'
const UserVideoStreams = require('../models/UserVideoStreams')

const userIDParam = {
  name: 'user_id',
  in: 'path',
  description: 'User ID',
  type: 'string',
  required: true,
  example: 'usr_4456'
}

module.exports = [
  {
    method: 'GET',
    url: '/users/:user_id/streams',
    summary: 'List streams for a user',
    description: 'Get a list of streams for an individual user',
    parameters: [
      userIDParam
    ],
    responses: {
      200: {
        description: 'Success',
        schema: {
          type: 'array',
          items: {
            type: 'string'
          }
        },
        examples: {
          'application/json': [
            '500',
            '5024',
            '100'
          ]
        }
      },
      default: {
        description: 'Error',
        schema: {
          '$ref': '#/definitions/Error'
        }
      }
    },
    handler: (req, res, next) => {
      const { user_id } = req.params

      const streams = new UserVideoStreams(user_id)
      streams.list()
        .then(result => next(null, result))
        .catch(next)
    }
  },
  {
    method: 'POST',
    url: '/users/:user_id/streams',
    summary: 'Add a stream',
    description: 'Add a new stream for a user when they start watching it, or ping it to show it\'s still active',
    parameters: [
      userIDParam,
      {
        in: 'body',
        name: 'Stream',
        description: 'The stream to add/ping',
        schema: {
          type: 'object',
          required: [ 'stream_id' ],
          properties: {
            stream_id: {
              type: 'string'
            }
          },
          example: {
            stream_id: '1005'
          }
        }
      }
    ],
    responses: {
      201: {
        description: 'Success (new stream)',
        schema: {
          type: 'object',
          required: [ 'added' ],
          properties: {
            added: {
              type: 'boolean'
            }
          }
        },
        examples: {
          'application/json': {
            added: true
          }
        }
      },
      200: {
        description: 'Success (updated/ping)',
        schema: {
          type: 'object',
          properties: {
            added: {
              type: 'boolean',
              example: true
            }
          }
        },
        examples: {
          'application/json': {
            added: false
          }
        }
      },
      default: {
        description: 'Error',
        schema: {
          '$ref': '#/definitions/Error'
        }
      }
    },
    handler: (req, res, next) => {
      const { user_id } = req.params
      const { stream_id } = req.body

      const streams = new UserVideoStreams(user_id)
      streams.add(stream_id)
        .then(wasAdded => {
          if (wasAdded) {
            res.status = 201
          }
          next(null, { added: wasAdded })
        })
        .catch(next)
    }
  },
  {
    method: 'DELETE',
    url: '/users/:user_id/streams/:stream_id',
    summary: 'Delete a stream',
    description: 'Delete a stream when a user stops watching it',
    parameters: [
      userIDParam,
      {
        name: 'stream_id',
        in: 'path',
        description: 'Stream ID',
        example: '1005',
        type: 'string',
        required: true
      }
    ],
    responses: {
      200: {
        description: 'Success',
        schema: {
          type: 'object',
          required: [ 'deleted' ],
          properties: {
            deleted: {
              type: 'boolean'
            }
          }
        }
      }
    },
    handler: (req, res, next) => {
      const { user_id, stream_id } = req.params

      const streams = new UserVideoStreams(user_id)
      streams.delete(stream_id)
        .then(wasDeleted => {
          next(null, { deleted: wasDeleted })
        })
        .catch(next)
    }
  }
]
