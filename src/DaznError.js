'use strict'

const ERRORS = {
  internal: {
    status: 500,
    message: 'Internal server error',
    description: 'Something went wrong with processing this request. If it continues, please contact us'
  },
  route_not_found: {
    status: 404,
    message: 'Route not found',
    description: 'No route was found here, please check the docs for valid routes'
  },
  stream_limit_reached: {
    status: 403,
    message: 'Stream limit reached',
    description: 'You cannot watch more than 3 streams at a time'
  },
  stream_not_found: {
    status: 404,
    message: 'Stream not found',
    description: 'No stream was found with this ID'
  }
}

class DaznError extends Error {
  constructor(code, extra = {}) {
    const err = ERRORS[code]
    if (!err) {
      throw new ReferenceError('Unknown error code: ' + code)
    }

    super(err.message)

    this.code = code
    this.status = err.status
    this.description = err.description
    this.extra = extra
  }

  static get errors() {
    return ERRORS
  }

  static handleAPIError(err, req, res) {
    // check if this is an error we handled
    if (!(err instanceof this)) {
      if (err.status === 404) {
        err = new DaznError('route_not_found', { url: req.url })
      } else {
        err = new DaznError('internal')
      }
    }

    res.status = err.status
    return err
  }

  toJSON() {
    return {
      code: this.code,
      status: this.status,
      message: this.message,
      description: this.description,
      extra: this.extra
    }
  }
}

module.exports = DaznError
