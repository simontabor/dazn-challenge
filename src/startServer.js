'use strict'
const server = require('./server')
server.start(() => {
  console.log(`Server running on ${server.opts.port}`)
})
