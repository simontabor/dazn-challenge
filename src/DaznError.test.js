'use strict'
const DaznError = require('./DaznError')

describe('DaznError', () => {

  describe('constructor', () => {
    it('throws on unknown/unconfigured errors', () => {
      expect(() => new DaznError('no')).toThrow(ReferenceError)
    })

    it('has a stack defined', () => {
      const err = new DaznError('internal')
      expect(err.stack).toEqual(expect.any(String))
    })
  })

  describe('.errors', () => {
    it('returns an object with defined errors', () => {
      expect(DaznError.errors).toEqual(expect.any(Object))
      expect(DaznError.errors.internal).toEqual(expect.objectContaining({
        status: 500,
        message: expect.any(String),
        description: expect.any(String)
      }))
    })
  })

  describe('.handleAPIError', () => {
    it('returns internal error for unhandled errors', () => {
      expect(DaznError.handleAPIError(new Error('test'), {}, {})).toEqual(new DaznError('internal'))
    })

    it('returns route_not_found error for 404 errors', () => {
      expect(DaznError.handleAPIError({ status: 404 }, { url: '/test' }, {})).toEqual(new DaznError('route_not_found', { url: '/test' }))
    })

    it('returns the same DaznError when called with one', () => {
      const err = new DaznError('stream_limit_reached')
      expect(DaznError.handleAPIError(err, {}, {})).toBe(err)
    })
  })

  describe('#toJSON', () => {
    const err = new DaznError('internal', { additional: 'information' })
    it('returns formatted error object', () => {
      expect(err.toJSON()).toMatchSnapshot()
    })
  })
})
